#### read first line of a file

Create a file called `readme.txt` in the same folder and make this java file called `Reader.java`.\
Execute it with `java Reader.java`:
```
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Reader {
  public static void main(String[] args) throws FileNotFoundException {
    File myfile = new File("readme.txt");

    Scanner myscanner = new Scanner(myfile);

    System.out.println(myscanner.nextLine());

    myscanner.close();
  }
}
```

#### read the entire file

```
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Reader {
  public static void main(String[] args) throws FileNotFoundException {
    File myfile = new File("readme.txt");

    Scanner myscanner = new Scanner(myfile);

    while(myscanner.hasNextLine()) {
      System.out.println(myscanner.nextLine());
    }

    myscanner.close();
  }
}
```

#### store entire file in a string variable

```
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Reader {
  public static void main(String[] args) throws FileNotFoundException {
    File myfile = new File("readme.txt");

    Scanner myscanner = new Scanner(myfile);

    String mystring = "";

    while(myscanner.hasNextLine()) {
      mystring = mystring.concat(myscanner.nextLine() + "\n");
    }

    myscanner.close();

    System.out.println(mystring);
  }
}
```

#### substrings

open `jshell`:
```
jshell> String hw = "hello world";
hw ==> "hello world"

jshell> System.out.println(hw.substring(2,5));
llo
```


#### splitting strings

I have again the String `hw` which contains `hello world`:
```
jshell> hw
hw ==> "hello world"
```

The function `.split()` requires a String argument and gives back an array.
```
jshell> hw.split(" ");
$8 ==> String[2] { "hello", "world" }
```

So I can declare and assign an array like this:
```
jshell> String[] hw_split = hw.split(" ");
hw_split ==> String[2] { "hello", "world" }
```

```
jshell> System.out.println(hw_split[0]);
hello
```

```
jshell> System.out.println(hw_split[1]);
world
```

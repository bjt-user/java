This only worked once:

To compile and execute a java project with a `.jar` file as a library you should try
```
javac -cp lib.jar Hello.java
java -cp lib.jar Hello
```
Note that the `.jar` needs to be in the same directory as the Hello.java file otherwise you would need a slightly different command.


#### inspecting jar files

```
jar -tf foo.jar
```

You can also just unzip a jar file:
```
unzip foo.jar
```

or to just simulate unzipping and watching all the files:
```
unzip -l foo.jar
```


#### troubleshooting

I tried this a second time with a different file and lib and it didnt work.
```
Error: Could not find or load main class hello
Caused by: java.lang.ClassNotFoundException: hello
```

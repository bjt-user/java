#### What is Apache Maven?

`Apache Maven` downloads libraries, so you don't have to download a java library with a web browser,\
put it into some folder and set the `classpath` for compiling.\
(The `classpath` is the path where additional classes for you java application are located.\
The classpath should only be necessary if the classes are not in the same folder.) \
So it is also a build tool.

TODO: make a page for classpath and link to it

A `maven` project is configured in a file called `pom.xml`.\
In that file the libraries that need to be downloaded (`dependencies`) are referenced.

In a `maven` project you usually have a `src` and a `target` folder.\
The `src` fodler contains the `.java` files and the `target` folder contains the `.class` files that have been comiled.\
Inside the `src` is a `main` folder where your code is in and a `test` folder for `junit tests`.\
Inside the `main` and the `test` folder is another folder named `java` and inside that are the `.java` files.\
(pretty unnecessary but whatever)


#### installation (command line)

```
$ wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.tar.gz
ERROR: cannot verify dlcdn.apache.org's certificate, issued by '/C=US/O=Let\'s Encrypt/CN=R3':
Issued certificate has expired.
To connect to dlcdn.apache.org insecurely, use `--no-check-certificate'.
```
(no idea what this is, but I used `--no-check-certificate` as workaround)
```
wget --no-check-certificate https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.tar.gz
```
```
tar -xvzf apache-maven-3.8.4-bin.tar.gz
```

put this in your `$PATH` (change this to whereever you have untarred the package):\
[how to set environment variables and $PATH](https://gitlab.com/bjt-user/java/-/blob/main/installation.md#setting-environment-variables)
```
/home/myuser/opt/apache-maven-3.8.4/bin
```
now you can type
```
which mvn
```
or
```
mvn -version
```

#### create pom.xml

I did not find a way yet to create a pom.xml on the command line.\
It looks like you have to write that file manually...

#### cannot find main class (java.lang.ClassNotFoundException...)
A common error I got is the classnotfoundexception.\
First of all you need to have the right folder structure.

This time the solution was to have this in your mainclass:\
```
package <mygroupid>.<folderaftergroupid>;
```
So you have to name the two folders after the `java` folder.\
(this time the java language server changed this after I organized the folders wrong\
and still showed an error after I corrected that, so I had to reopen the file\
and then it showed no more error)

But the next time I opened the project and tried to compile the error is there again...\
And then I tried again and it worked...maybe it is luck based because of the proxy...

#### Maven Project in Eclipse (FAIL)

File -> New -> Project -> type `maven` into the search field\
Select `Maven Project`. -> `Next`\
Tick `create simple project (skip archetype selection)` -> `Next`\
Now you have to enter a `group id` and `Artifact id`...\
The `Artifact ID` seems to be something like a project name, so I just use the Project name.\
`group id` I use the initials of my name or you could use your company name.\
`Finish`

And it created a skeleton for my project with a `src/main/java` directory structure.\
Looked fine at first sight but on the right bottom side of Eclipse there is never ending progress bar at 0% that says `creating project ...`.\

#### proxy

`mvn` should pick up the environment variable `JDK_JAVA_OPTIONS`:
```
NOTE: Picked up JDK_JAVA_OPTIONS: -Dhttps.proxyPort=3128 -Dhttps.proxyHost=10.101.11.10
```

You can also use a special argument with the mvn command.

Configuring the proxy properties in a settings.xml did not work, maybe it needs to be configured slightly different.

I tried putting this
```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                  http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <proxies>
    <proxy>
      <active>true</active>
      <protocol>http</protocol>
      <host>proxy.mycompany.deu</host>
      <port>3128</port>
      <nonProxyHosts>localhost|10.101.11.27</nonProxyHosts>
    </proxy>
  </proxies>
</settings>
```
into `~/.m2/settings.xml` to get rid of Eclipse Errors => didnt work

#### downsides of maven

You need to have the right folder structure otherwise it will not work.\
But should be able to configure maven to use another folder structure.

#### download sources

trying to enable ctrl-clicking\
try this:\
https://stackoverflow.com/questions/11361331/how-to-download-sources-for-a-jar-with-maven

#### maven-compiler-plugin

What is it and how do I know what version I have?\
Because you need the version for the pom.xml.

#### make project into ONE executable jar

https://www.baeldung.com/executable-jar-with-maven

=> try way number 2.2

put this in your pom.xml:
```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-assembly-plugin</artifactId>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>single</goal>
            </goals>
            <configuration>
                <archive>
                <manifest>
                    <mainClass>
                        com.baeldung.executable.ExecutableMavenJar
                    </mainClass>
                </manifest>
                </archive>
                <descriptorRefs>
                    <descriptorRef>jar-with-dependencies</descriptorRef>
                </descriptorRefs>
            </configuration>
        </execution>
    </executions>
</plugin>
```
You have to provide your main class with the fully qualified path/package path...

then you do: 
```
mvn package
```

but a
```
mvn clean package
```
is better because it seems to make sure, that everything is recompiled

But I can't execute the jar file that comes out:
```
no main manifest attribute
```

https://stackoverflow.com/questions/9689793/cant-execute-jar-file-no-main-manifest-attribute#9689877

to make a jar executable... you need to jar a file called `META-INF/MANIFEST.MF`

the file itself should have (at least) this one liner:

Main-Class: com.mypackage.MyClass

so I go into the directory where the pom.xml is and do:
```
mkdir META-INF
touch META-INF/MANIFEST.MF
vim META-INF/MANIFEST.MF
```
and put in `Main-Class: com.mypackage.MyClass` (but changed the class accordingly)

And now I have an executable in the `target` folder called `something-helloworld-1.0-SNAPSHOT-jar-with-dependencies.jar` that I can execute with
```
java -jar something-helloworld-1.0-SNAPSHOT-jar-with-dependencies.jar
```

=> this worked great!

***
#### local repository and online repository

Maven will first look at your local repository in `~/.m2/repository` and then search the online repository.\
https://www.baeldung.com/maven-clear-cache

You can just delete the `repository` folder to clear the local cache and to make maven download the dependencies from the internet again.

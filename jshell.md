You can use the `.jsh` file extension to run `jshell` scripts.

It is a kind of java scripting language where you don't need classes and all that stuff that makes java complicated.\
https://en.wikipedia.org/wiki/JShell

#### interactive jshell

type `jshell` to get into the `jshell`:
```
jshell> System.out.println("hello");
hello
```

type
```
/list
```
to see what you have executed so far

```
/vars
```
to see all variables that have been declared so far

exit `jshell`:
```
/exit
```

```
/save mycode.jsh
```
This will save everything that is in `/list` to a file in the current directory.\
(but it does not include `/exit` at the end)

***

#### execute jshell scripts

I wrote this file called `test.jsh`
```
System.out.println("hello");

/exit
```

Execute it like that:
```
jshell test.jsh
```


#### tab completion

type
```
String hw = "hello world";
```
then type
```
hw.
```
and hit <kbd>tab</kbd> to get possible functions you can use with that String

Then you can enter the function and hit <kbd>tab</kbd> again to see which arguments it needs.
```
jshell> System.out.println(hw.split(
hw   

Signatures:
String[] String.split(String regex, int limit)
String[] String.split(String regex)

<press tab again to see documentation>
jshell> System.out.println(hw.split(
String[] String.split(String regex, int limit)
<no documentation found>

<press tab to see next documentation>
```

#### imports

By default, JShell automatically imports a few useful java packages when the JShell session is started.\
We can type the command `/imports` to get a list of all these imports.

```
jshell> /imports
|    import java.io.*
|    import java.math.*
|    import java.net.*
|    import java.nio.file.*
|    import java.util.*
|    import java.util.concurrent.*
|    import java.util.function.*
|    import java.util.prefs.*
|    import java.util.regex.*
|    import java.util.stream.*
```

#### add a jar to the classpath

look at the current classpath like this:
```
/env
```

add a jar to the classpath if you are in the directory of the jar like this:
```
/env --class-path myjarfile.jar
```

#### proxy

`jshell` does not seem to load Java environment variables on default.

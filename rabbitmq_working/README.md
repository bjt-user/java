Make sure, `rabbitmq` is running.

Run the file with:
```
jbang camelrabbitmq.java
```

Then you can type `localhost:15672` in firefox.\
Go to `queues` and see that the `in` queue (coded in `from`-option) was created.\
Click `get messages` and select how many messages you want to see.\
The java application keeps running and keeps sending the message `hello camel`.\
Those messages should show up in the queue.

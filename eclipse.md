#### prevent eclipse from opening donation page on open

on Ubuntu Eclipse will open a webbrowser with a donation page everytime you start Eclipse.\
change the preferences (Window->Preferences->General->Web Browser) to add a new external web browser and set it's location as `/usr/bin/true`.\
Then set this as the default browser.

#### code completion

Window -> Preferences -> Java -> Editor -> Content Assist\
Put all letters into this field, so that code completion activates after each letter typed:
```
Auto activation triggers for Java: .abcdefghijklmnopqrstuvwxyz
```

Next: Tick `disable insertion triggers except enter`\
(otherwise your code will be completed even though you don't want it)

Now you can start typing and you will get proposals by eclipse.\
You can choose one by hitting `<enter>`.


#### snippets

Snippets are called `templates` in Eclipse.\
Window -> Preferences -> Java -> Editor -> Templates\
Here are all the default snippets with their aliases, and you can add custom snippets.

For example there is the default snippet for `System.out.println()`.\
You can type `syso` and if you have the `auto activation triggers` set like shown [here](#code completion)\
then the autocompletion will trigger and propose `sysout` then press `<enter>` the snippet will be placed.


#### keyboard shortcuts

Window -> Preferences -> General -> Keys

You can sort by binding, command, or category or type in a search text to find specific key bindings.\
Select the desired row, click into `binding:` field and hit your desired key.

For example I set the `run` command to `<F5>`.\
(note that there are two run commands and one of them opens a menu)

When there are conflicts you can select a row and hit `unbind` button.

After your changes hit `Apply and close`.

#### terminal
to open a Windows (cmd) terminal:
```
ctrl + alt + t
```
But that doesn't open in the directory of the current project...

To get that you need to right-click on the project -> `show in local terminal -> terminal`\
or\
select the project by clicking it with the left mousebutton and then hit `ctrl + alt + t`


#### package explorer

The package explorer shows your projects on the left hand side.\
This does not represent a directory in the system.\
It shows projects that you created or opened in Eclipse.

To get rid of projects you can go to the three dots (view menu) and select `filter`.\
Then tick `closed projects`.\
Then you can right click on a project and select `close project` and will not be shown anymore.

#### editor settings

change **tab width** and **indentation sytle** like this:\
Window > Preferences > Java > Code Style > Formatter

And then you have to make a new profile or change the current one.

***

## eclipse and linux

#### installation with flatpak
There is a graphical installer for Linux (which I never seen before...) but you should avoid that by any means!\
Try to use flatpak!

https://flathub.org/apps/details/org.eclipse.Java
```
flatpak install flathub org.eclipse.Java
```
this worked for Debian\
on Ubuntu my flatpak is too old\
```
Error: org.eclipse.Java needs a later flatpak version
error: Failed to install org.eclipse.Java: app/org.eclipse.Java/x86_64/stable needs a later flatpak version (1.7.1)

$ flatpak --version
Flatpak 1.6.5
```
Then I can hit <kbd>super</kbd> and type `eclipse`.

#### extracting a package in /opt

I remember that you could just download a package in Windows and extract that and the installation was finished.\
(without using an installer)\
That should work for Linux too.

https://www.eclipse.org/downloads/packages/

=> untarred a Linux package in `/opt/`. Added the folder with the executable to the PATH.\
Put I cant start it via gnome only via terminal.\
I think `i3` uses the path for starting applications, but `gnome` uses `Desktop Entries`.

> Desktop files may be created in either of two places:

> `/usr/share/applications/` for desktop entries available to every user in the system
> `~/.local/share/applications/` for desktop entries available to a single user

https://unix.stackexchange.com/questions/103213/how-can-i-add-an-application-to-the-gnome-application-menu

So I put this into `usr/share/applications/eclipse.desktop`:
```
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Eclipse for Java
Comment=Application description
Icon=/opt/eclipse/icon.xpm
Exec=/opt/eclipse/eclipse
Terminal=false
Categories=Tags;Describing;Application
```

Then **close** the **terminal window**!!!\
Then hit <kbd>super</kbd> and it should work.

#### hello world

there is a guide inside eclipse to make a hello world java application.\
And when you select `use project folder as root for source and class files` you dont get the annoying maven/gradle folder structure.

***

#### plugins

A popup arrived and asked me to install maven plugins. I accepted and now I have annoying warning.\
Where do I manage and remove plugins?

help -> about eclipse ide -> installation details

looks like you cant uninstall the stuff that was installed by default

***
***
***

## troubleshooting

Nach dem Erstellen eines maven Projekts werden direkt 3 Warnings angezeigt, bevor überhaupt eine Datei angelegt wurde oder Code geschrieben wurde.

```
Build path specifies execution environment J2SE-1.5.
There are no JREs installed in the workspace that are strictly compatible with this environment.
```

1. try: setting jdk in ~/.m2/settings.xml => still warnings...

=> compare with what is installed at home

***

getting `main class not found` errors when creating a new project in eclipse...\
=> then I tried it again with java 17 and it worked...wtf is this???\
maybe the "workspace" has java 17 configured???

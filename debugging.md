I have a maven project that I can compile and run in debug mode like this:
```
mvnDebug package exec:java -D"exec.mainClass"="..."
```
But it does not open a debugger, it says that I can attach a debugger on port 8000:
```
Preparing to execute Maven in debug mode
Listening for transport dt_socket at address: 8000
```

Maybe I can use `jdb` on the cli in another shell instance on the same server to debug on port 8000.\
Remote debugging with eclipse did not work, timeout, probably firewall blocking.

```
$ jdb
-bash: jdb: command not found
```

But you can't install `jdb` with `dnf`.

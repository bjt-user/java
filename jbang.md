JBang lets Students, Educators, and Professional Developers create, edit and run self-contained source-only or binary Java programs with unprecedented ease.\
JBang's goal is to reduce or even remove the ceremony and tedious setup that everyone is so used to around Java.

#### installation (windows)

on **Windows** using **PowerShell**, we can use iex:
```
iex "& { $(iwr https://ps.jbang.dev) } app setup"
```
Output:
```
PowerShell requires an execution policy in [Unrestricted, RemoteSigned, ByPass] to continue.                                                                                                                       For example, to set the execution policy to 'RemoteSigned' please run :                                                                                                                                            'Set-ExecutionPolicy RemoteSigned -scope CurrentUser'
```

So I run:
```
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
```
Answer with `A` (yes to all).

Do this again:
```
iex "& { $(iwr https://ps.jbang.dev) } app setup"
```
Output:
```
Downloading JBang...
Installing JBang...
[jbang] Setting up JBang environment...
Please start a new PowerShell for changes to take effect
```

#### installation (linux)

```
curl -Ls https://sh.jbang.dev | bash -s - app setup
```
output:
```
Downloading JBang...
Installing JBang...
[jbang] Setting up JBang environment...
Please start a new Shell for changes to take effect
```
After starting a new shell I have this in my `PATH`:
```
/home/bf/.jbang/bin:
```

#### Usage

Now I can do
```
jbang HelloWorld.java
```
and the java program is executed.

This will produce a helloworld java file 
```
jbang init hello.java
```
You can even just execute that file with `./hello.java`.

It looks like you just write the dependencies in your javafile like this:
```
//DEPS [gradle short annotation]
```

This is the header:
```
///usr/bin/env jbang "$0" "$@" ; exit $?
```
The header is optional. But if you want to make the file executable you need it.

To get IDE features like import proposals, javadoc and ctrl-clicking into source, do this:
```
jbang edit hello.java
```
Then it asks you if you want to open it in VSCode, type `2`.\
Then a folder structure is being created and everything should work.\
It creates the folder structure in the .jbang directory.\
But when you save the file it is also saved in the actual directory.\
So this might not even be a problem.

If you need a **specific java version**, do this:
```
jbang --java 15 xyz.java
```
You can also specify in the header that you want this app to run with at least java xyz...

You can also do
```
jbang [githublink]
```

For more info:
https://www.youtube.com/watch?v=gVL-CFEOGs8

#### proxy

It does not work out of the box with a corporate proxy.\
Putting a `settings.xml` in the folder or in `.m2` folder **doesn't help**.

Then I tried setting these two environment variables as suggested here:\
https://docs.oracle.com/javase/8/docs/technotes/guides/net/proxies.html
http.proxyHost: the host name of the proxy server\
http.proxyPort: the port number, the default value being 80\
=> did **not work**

Setting the `JAVA_TOOL_OPTIONS` environment variable did **not work**.\
```
Picked up JAVA_TOOL_OPTIONS: "-Dhttp.proxyHost=proxy.cpbs.deu -Dhttp.proxyPort=3128 -Dhttps.proxyHost=proxy.cpbs.deu -Dhttps.proxyPort=3128"
```

#### jbang at home

you can test jbang like this: (at home jbang works)\
install:
```
curl -Ls https://sh.jbang.dev | bash -s - app setup
```

make a file called `try.java` with the following content:
```
///usr/bin/env jbang "$0" "$@" ; exit $?
//DEPS com.github.lalyos:jfiglet:0.0.8

import com.github.lalyos.jfiglet.FigletFont;

class hello {

    public static void main(String... args) throws Exception {
        System.out.println(FigletFont.convertOneLine(
               "Hello " + ((args.length>0)?args[0]:"jbang")));  ;;
    }
}
```

execute it with `jbang try.java`

***
***
***
#### fails, cons and open questions

It doesn't work under corporate proxy.

I tried using JBang for a VSCode Extension called Karavan for Apache Camel, but it didnt work.

`java 1.8.x` means `java 8`


On Fedora you can configure which java version to use with this command:
```
sudo update-alternatives --config java
```

To see which java versions are available in dnf and which ones are installed do:
```
sudo dnf list *openjdk*
```

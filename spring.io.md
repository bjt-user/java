The website https://spring.io/ claims to make java programming easier.

Try this to start: https://spring.io/quickstart

https://start.spring.io/ leads to a configuration formula that creates your project.

You can save ("share") configurations in a link:\
https://start.spring.io/#!type=gradle-project&language=java&platformVersion=2.6.4&packaging=jar&jvmVersion=11&groupId=com.example&artifactId=demo&name=demo&description=Demo%20project%20for%20Spring%20Boot&packageName=com.example.demo&dependencies=camel

After hitting `generate` a `.zip` file will be downloaded.

=> gradle project did not even run (probably no proxy configured)

=> I could ran a maven project with the command `mvn package` and no further arguments,\
but the app didn't do anything except print a `spring` logo...

Spring does not seem to have any use at all.

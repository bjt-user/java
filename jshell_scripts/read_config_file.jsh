// this script assumes, that you have a file called "readme.txt" in the same folder
// and in that file there are the strings "queue1" and "queue2" which have a name configured in them
// like this:
// queue1: in
// queue2: out
// (if the user enters leading or trailing blanks, they will be removed by the trim() function)
// note: the Properties class seems to be located at java.util.Properties
// you will also need these imports for a classic java program:
// import java.util.Properties;
// import java.io.File;
// import java.io.FileInputStream;
// import java.io.FileNotFoundException;
// import java.io.IOException;

Properties prop = new Properties();
String fileName = "readme.txt";

try (FileInputStream fis = new FileInputStream(fileName)) {
    prop.load(fis);
} catch (FileNotFoundException ex) {
    // this catch is optional
} catch (IOException ex) {
    // ?
}

System.out.println(prop.getProperty("queue1"));
System.out.println(prop.getProperty("queue2"));

String queue1 = prop.getProperty("queue1").trim();
String queue2 = prop.getProperty("queue2").trim();

System.out.println("queue1 is" + queue1 + ".");
System.out.println("queue2 is" + queue2 + ".");

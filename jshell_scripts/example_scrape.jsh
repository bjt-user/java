// a script that scrapes the website example.com with jsoup

/env --class-path jsoup-1.15.3.jar

import org.jsoup.*;
import org.jsoup.nodes.*;

Document doc = Jsoup.connect("http://example.com/").get();
System.out.println("this is the entire document:");
System.out.println(doc + "\n");
String title = doc.title();
System.out.println("this is the title of the document: " + title);
System.out.println("this is the type of the document: " + doc.documentType());

/exit

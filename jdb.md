java debugger

You can use it on the cli on bytecode.

```
$ ls -la
total 8
drwxr-xr-x. 1 bf bf  30 Mar 11 11:34 .
drwxr-xr-x. 1 bf bf 184 Mar 11 11:25 ..
-rw-r--r--. 1 bf bf 399 Mar 11 11:34 hw.class
-rw-r--r--. 1 bf bf  97 Mar 11 11:27 hw.java
$ jdb hw
Initializing jdb ...
> 
```

#### compile with debugging symbols

You should compile to bytecode with debugging symbols, so you can use commands like `locals` and print your variables.

```
javac -g hw.java
```
***

#### set breakpoints

You can type `run` but that will just execute the program and quit jdb...

You have to set a breakpoint with `stop at [classname]:[linenumber]`
```
$ jdb hw
Initializing jdb ...
> stop at hw:3
Deferring breakpoint hw:3.
It will be set after the class is loaded.
> run
run hw
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: Set deferred breakpoint hw:3

Breakpoint hit: "thread=main", hw.main(), line=3 bci=0
3        System.out.print("hi\n");

main[1] list
1    public class hw {
2      public static void main(String[] args) {
3 =>     System.out.print("hi\n");
4    
5        System.out.print("and goodbye\n");
6      }
7    }
main[1]
```

#### step through code and print variables

use `step` to step through code and `locals` to print all your local variables.

Or `print myvar` to print the variable myvar.

`stepi` is "step into"

`next` is "step over"

***

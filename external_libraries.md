I did not find a way to use external libraries yet.

continue this vid at 2:00 (first two minutes were just crap)
https://www.youtube.com/watch?v=e0GYa7_cToo&ab_channel=NathanielOsgood

#### find libraries

https://mvnrepository.com/search?q=camel+main

#### proxy problems
The problem with build tools like Maven, Ant or Gradle that you need to tell it to go through the proxy if you have a proxy.

#### jump into library source code
Then there is the question how do you jump into the source code of that external library?\
How to enable ctrl-clicking Classes and Methods in VS Code?

SO quote:\
"In order to jump into source code, the corresponding source code JARs need to be downloaded resp. be part of the project setup dependencies. Similar for the javadocs, they came in a separate JAR, too. So, IF the maven setup is correct, and IF the eclipse project makes proper usage of everything, things should work."

Solution:\
**Using ctrl-click on library/framework methods/classes worked after installing a java language server in vscode. (from Redhat)**\
**I installed a lot of other recommended extensions with that. (from Microsoft mainly)**


#### example libraries

https://jsoup.org/download

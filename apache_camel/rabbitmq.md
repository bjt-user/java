https://camel.apache.org/components/3.15.x/rabbitmq-component.html



Maybe try this:\
https://github.com/lshift/camel-rabbitmq/tree/master/src/main/java/net/lshift/camel/component/rabbitmq


dependencies:\
```
// https://mvnrepository.com/artifact/org.apache.camel/camel-rabbitmq
implementation 'org.apache.camel:camel-rabbitmq:3.15.0'
```

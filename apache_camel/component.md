A Component is essentially a factory of Endpoint instances.


Camel components are configured on two separate levels:

- component level
-  endpoint level

The component level is the highest level which holds general and common configurations that are inherited by the endpoints.\
For example a component may have security settings, credentials for authentication, urls for network connection and so forth.

Configuring components can be done with the Component DSL, in a configuration file (application.properties|yaml), or directly with Java code.

https://camel.apache.org/manual/component.html

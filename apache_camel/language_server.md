there is a language server for visual studio code.\
that helps with `.from` and `.to` statements but not much for the general project.

#### eclipse

https://github.com/camel-tooling/camel-lsp-client-eclipse

I just dragged the install icon from the readme on github into my workspace in eclipse and it installed the language server.\
You can see language servers in eclipse in `Window->Preferences->Language Servers`.

=> but it doesnt seem to help much

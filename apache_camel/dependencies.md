https://camel.apache.org/manual/what-are-the-dependencies.html

Camel JAR Dependencies

Camel core itself is lightweight, and only requires the slf4j-api logging API jar.\
Gradle Short:
```
// https://mvnrepository.com/artifact/org.slf4j/slf4j-api
implementation 'org.slf4j:slf4j-api:2.0.0-alpha7'
```
Components

All the Components have a range of 3rd party jars they depend on. They are listed in the maven pom files which files they require.

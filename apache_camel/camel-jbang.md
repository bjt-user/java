https://camel.apache.org/manual/camel-jbang.html

Maybe try it with yaml based routes.

```
jbang app install camel@apache/camel
```

Then you can create a `.yaml` file and do
```
camel run myroute.yaml
```

But I couldnt set an URI correctly.

https://camel.apache.org/manual/uris.html

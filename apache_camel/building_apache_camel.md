You can't download a `jar` file.

You can only download the git repo.\
You should be able to build the project and make it into a `jar` file and then use that for your project.

https://camel.apache.org/download/

https://camel.apache.org/manual/building.html


```
export MAVEN_OPTS="-Xms2048m -Xmx3584m"

mvn clean install -Pfastinstall
```

A lot of warnings. Website says it takes 10 minutes.\
=> abgebrochen nach ca. 20min



If you want to build jar files with the source code, that for instance Eclipse can important, so you can debug the Camel code as well. Then you can run this command from the camel root folder:

```
mvn clean source:jar install -Pfastinstall
```


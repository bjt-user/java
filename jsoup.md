https://jsoup.org/

https://github.com/jhy/jsoup


You can directly download jars:\
https://jsoup.org/download

You should be able to download html by giving a URL and parse it.

It might also make sense to learn about `DOM` and the structure of html documents.\
https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction


#### using jsoup in jshell

If you are in the folder where your jar is you can do:
```
jshell> /env --class-path jsoup-1.15.3.jar
|  Setting new options and restoring state.
```

show the classpath:
```
jshell> /env
|     --class-path jsoup-1.15.3.jar
```

But what does the `import` statement look like?

For the first example https://jsoup.org/cookbook/introduction/parsing-a-document I did it like this:
```
jshell> String html = "<html><head><title>First parse</title></head>"
  + "<body><p>Parsed HTML into a doc.</p></body></html>";
```

When I enter the second line of code `jshell` can't find two symbols:
```
jshell> Document doc = Jsoup.parse(html);
|  Error:
|  cannot find symbol
|    symbol:   class Document
|  Document doc = Jsoup.parse(html);
|  ^------^
|  Error:
|  cannot find symbol
|    symbol:   variable Jsoup
|  Document doc = Jsoup.parse(html);
|                 ^---^
```

So I did a `jar -tf` on the jsoup jar file and grepped after those symbols.
```
$ jar -tf jsoup-1.15.3.jar | grep -i "Document.class"
org/jsoup/nodes/Document.class
```
```
$ jar -tf jsoup-1.15.3.jar | grep -i "jsoup.class"
org/jsoup/Jsoup.class
```

**note**: the import statements don't have the `.class` part in them!

```
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
```

And then the first example seemed to work. It just formatted the html String a little.

I converted this:
```
jshell> System.out.println(html);
<html><head><title>First parse</title></head><body><p>Parsed HTML into a doc.</p></body></html>

```

into this:
```
jshell> System.out.println(doc);
<html>
 <head>
  <title>First parse</title>
 </head>
 <body>
  <p>Parsed HTML into a doc.</p>
 </body>
</html>
```

> Once you have a Document, you can get at the data using the appropriate methods in Document and its supers Element and Node.

***

#### about webscraping in general

Pages that might be easy or useful to scrape for practicing:\
https://www.bloomberg.com/markets/stocks \
=> Nope, after a `curl` (that was used cause of proxy) this came out:

We've detected unusual activity from your computer network

To continue, please click the box below to let us know you're not a robot.\
Why did this happen?

Please make sure your browser supports JavaScript and cookies and that you are not blocking them from loading.\
For more information you can review our Terms of Service and Cookie Policy.

=> try again at home with `jsoup` to get the html

https://stackoverflow.com/a/63677280/13253079

=> you probably need to provide the cooky via `jsoup`

try this:\
https://www.youtube.com/watch?v=pMnEsdIFmmk

=> tried some stuff with putting a cookie that has the word `consent` into jsoup with the `.cookie` method, did not work...

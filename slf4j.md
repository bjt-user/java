Stands for simple logging facade for java.

This is the basic dependency:
```
//DEPS org.slf4j:slf4j-api:2.0.0-alpha7
```

But you will still get a warning:
```
SLF4J: No SLF4J providers were found.
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#noProviders for further details.
```

This warning is resolved when you also add this to your dependencies:
```
//DEPS org.slf4j:slf4j-simple:2.0.0-alpha7
```
=> and then your java app will output logging information to the screen

#### disable logging
If your framework needs `slf4j` but you don't want to use it you can also add the `noop` dependency.\
Then you won't get any warnings anymore and no logging.
```
// https://mvnrepository.com/artifact/org.slf4j/slf4j-nop
testImplementation 'org.slf4j:slf4j-nop:2.0.0-alpha7'
```

```
error: can't find main(String[]) method in class
```

The cause was:\
I forgot to put `String[] args` as an argument for the main method.\
The main method has to look like this in java:
```
public static void main(String[] args) {
	System.out.print("hello world");
}
```


#### unreported exception IOException
```
error: unreported exception IOException; must be caught or declared to be thrown
```

I could fix this by adding this to the main method:
```
public static void main(String[] args) throws IOException
```

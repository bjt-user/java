## how to set JAVA_HOME correctly?

It is probably a java folder that contains a bin folder and in that bin folder there must be a `java` executable.

this is `/usr/lib/jvm/java-17-openjdk-amd64/bin`:
```
drwxr-xr-x 2 root root  4096 Jul 18 19:40 .
drwxr-xr-x 7 root root  4096 Jul 18 19:40 ..
-rwxr-xr-x 1 root root 14496 Apr 24 16:03 java
-rwxr-xr-x 1 root root 14512 Apr 24 16:03 jpackage
-rwxr-xr-x 1 root root 14512 Apr 24 16:03 keytool
-rwxr-xr-x 1 root root 14512 Apr 24 16:03 rmiregistry
```
this was after a `sudo apt install openjdk-17-jre-headless`.


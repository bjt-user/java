## small example library

Trying to use the small library `jfiglet` to learn how to deal with java libraries.

https://github.com/lalyos/jfiglet

```
git clone https://github.com/lalyos/jfiglet
```

The `.java` source files are located at `~/Coding/java/jfiglet/jfiglet/src/main/java/com/github/lalyos/jfiglet`.

```
$ ls -1
FigletFont.java
JFiglet.java
Smushing.java
SmushingRule.java
SmushingRulesToApply.java
```

***

#### first fail

I can now do
```
javac *.java
```
to compile all `.java` files into `.class` files.

That seemed to work to produce one jar:
```
jar -cvf jfiglet.jar *.class
```

That jar file looks like that:
```
$ jar -tf jfiglet.jar
META-INF/
META-INF/MANIFEST.MF
FigletFont.class
JFiglet.class
Smushing.class
SmushingRule$1.class
SmushingRule$2.class
SmushingRule$3.class
SmushingRule$4.class
SmushingRule$5.class
SmushingRule$6.class
SmushingRule$7.class
SmushingRule$8.class
SmushingRule$Layout.class
SmushingRule$Type.class
SmushingRule.class
SmushingRulesToApply$ImproperUseException.class
SmushingRulesToApply$SmushingRuleCodeValueComparator.class
SmushingRulesToApply.class
```

I copied the file `jfiglet.jar` to a different folder so I could try to use it.

But I cant use it:
```
$ java -cp jfiglet.jar:. UseJfiglet.java
UseJfiglet.java:1: error: '.' expected
import JFiglet;
              ^
1 error
error: compilation failed
```

The jar file does **not** contain the `fully qualified path`, that is given in the `package` name in the source code:
```
package com.github.lalyos.jfiglet;
```

And when I give the fully qualified path in the import statement it still doesnt get found because the `fqp` is not in the jar file.

***

#### second fail

```
javac *.java
```
to compile all `.java` files into `.class` files.

now I **change** into the `java` **directory** and do:
```
jar -cvf jfiglet.jar com/github/lalyos/jfiglet/*.class
```

```
$ jar -tf jfiglet.jar 
META-INF/
META-INF/MANIFEST.MF
com/github/lalyos/jfiglet/FigletFont.class
com/github/lalyos/jfiglet/JFiglet.class
com/github/lalyos/jfiglet/Smushing.class
com/github/lalyos/jfiglet/SmushingRule$1.class
com/github/lalyos/jfiglet/SmushingRule$2.class
com/github/lalyos/jfiglet/SmushingRule$3.class
com/github/lalyos/jfiglet/SmushingRule$4.class
com/github/lalyos/jfiglet/SmushingRule$5.class
com/github/lalyos/jfiglet/SmushingRule$6.class
com/github/lalyos/jfiglet/SmushingRule$7.class
com/github/lalyos/jfiglet/SmushingRule$8.class
com/github/lalyos/jfiglet/SmushingRule$Layout.class
com/github/lalyos/jfiglet/SmushingRule$Type.class
com/github/lalyos/jfiglet/SmushingRule.class
com/github/lalyos/jfiglet/SmushingRulesToApply$ImproperUseException.class
com/github/lalyos/jfiglet/SmushingRulesToApply$SmushingRuleCodeValueComparator.class
com/github/lalyos/jfiglet/SmushingRulesToApply.class
```
=> this looks more like a fully qualified path

But when I try to embed it into a java application I get a weird error:
```
$ java -cp jfiglet.jar:. UseJfiglet.java
UseJfiglet.java:1: error: cannot access JFiglet
import com.github.lalyos.jfiglet.JFiglet;
                                ^
  bad class file: jfiglet.jar(/com/github/lalyos/jfiglet/JFiglet.class)
    class file has wrong version 61.0, should be 55.0
    Please remove or make sure it appears in the correct subdirectory of the classpath.
1 error
error: compilation failed
```

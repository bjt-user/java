Maybe this will work for webscraping...

https://www.selenium.dev/downloads/

click on `selenium for java`

```
unzip selenium-java-4.5.0.zip -d selenium-jars
```
it asks if it shall replace a file (why???) I answer with `A`.

https://www.selenium.dev/documentation/webdriver/getting_started/first_script/

#### create one jar

I put all jars that have the string "sources" in them in a folder.

```
jar -cf selenium-all-jars-sources.jar *
```

But the problem is, now I don't see the classes anymore when I look at the jar:
```
$ jar -tf selenium-all-jars-sources.jar 
META-INF/
META-INF/MANIFEST.MF
selenium-api-4.5.0-sources.jar
selenium-chrome-driver-4.5.0-sources.jar
selenium-chromium-driver-4.5.0-sources.jar
selenium-devtools-v104-4.5.0-sources.jar
selenium-devtools-v105-4.5.0-sources.jar
selenium-devtools-v106-4.5.0-sources.jar
selenium-devtools-v85-4.5.0-sources.jar
selenium-edge-driver-4.5.0-sources.jar
selenium-firefox-driver-4.5.0-sources.jar
selenium-http-4.5.0-sources.jar
selenium-ie-driver-4.5.0-sources.jar
selenium-java-4.5.0-sources.jar
selenium-json-4.5.0-sources.jar
selenium-remote-driver-4.5.0-sources.jar
selenium-safari-driver-4.5.0-sources.jar
selenium-support-4.5.0-sources.jar
```

And still no tab completion with `jshell`:
```
jshell> /env --class-path selenium-all-jars-sources.jar
|  Setting new options and restoring state.

jshell> import org.
graalvm.   ietf.      jcp.       w3c.       xml.       
jshell> import org.openqa

jshell> import org.openqa.
```

#### jshell import problem

I did an `/env --class-path` on a jar in `jshell`.\
But this time I can't do the import with tabcompletion like I could with `jsoup`...

maybe because I only added one jar of the framework to the class path.\
probably I need all jars. or make all jars into one.

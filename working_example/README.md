Make sure you have `jbang` installed.

Execute this program like this:
```
jbang camel.java
```

***

To enable logging messages appearing on the console simply add these two dependencies:
```
//DEPS org.slf4j:slf4j-api:2.0.0-alpha7
//DEPS org.slf4j:slf4j-simple:2.0.0-alpha7
```
